#define KILO(x) ((long)(x)*(long)1024L)
#define MEGA(x) ((long)1024 * KILO(x))

#define MIN(x, y) ((x) < (y) ? x : y)
#define MAX(x, y) ((x) > (y) ? x : y)
#define CLAMP(v, min, max) (MAX((min), MIN((max), (v))))

typedef struct {
	float x, y, w, h;	
} Rect;

typedef uint16_t DColor;

typedef struct {
	uint8_t r, g, b;
} RGBColor;

typedef struct {
	int32_t x, y;
} IVec2;

typedef union {
	struct {float x, y;};
	float array[2];
} Vec2;

typedef union {
	struct {float x, y, z;};
	struct {float r, g, b;};
	struct {Vec2 xy;};
	struct {float _x; Vec2 yz;};
	float array[3];
} Vec3;

static inline Vec3
vec3(float x, float y, float z) {
	return (Vec3){x, y, z};
}

static inline Vec3
vec3e(float e) {
	return (Vec3){e, e, e};
}

static Vec3
Vec3_add(Vec3 v0, Vec3 v1) {
	Vec3 result = {v0.x + v1.x, v0.y + v1.y, v0.z + v1.z};
	return result;
}

static Vec3
Vec3_sub(Vec3 v0, Vec3 v1) {
	Vec3 result = {v0.x - v1.x, v0.y - v1.y, v0.z - v1.z};
	return result;
}

static Vec3
Vec3_mul(Vec3 v0, Vec3 v1) {
	Vec3 result = {v0.x * v1.x, v0.y * v1.y, v0.z * v1.z};
	return result;
}

static Vec3
Vec3_div(Vec3 v0, Vec3 v1) {
	Vec3 result = {v0.x / v1.x, v0.y / v1.y, v0.z / v1.z};
	return result;
}

typedef union {
	struct {float x, y, z, w;};
	struct {float r, g, b, a;};
	struct {Vec3 xyz;};
	float array[4];
} Vec4;

typedef union {
	struct {Vec4 x, y, z, w;};
	struct {Vec3 xyz;};
	float m[4][4];
	float a[16];
} Mat4;

static Mat4
Mat4_mul(const Mat4 *a, const Mat4 *b)
{
	Mat4 c = {0};

	for (int i = 0; i < 4; i++) {
		c.m[0][i] += b->m[0][i] * a->m[0][0];
		c.m[0][i] += b->m[1][i] * a->m[0][1];
		c.m[0][i] += b->m[2][i] * a->m[0][2];
		c.m[0][i] += b->m[3][i] * a->m[0][3];

		c.m[1][i] += b->m[0][i] * a->m[1][0];
		c.m[1][i] += b->m[1][i] * a->m[1][1];
		c.m[1][i] += b->m[2][i] * a->m[1][2];
		c.m[1][i] += b->m[3][i] * a->m[1][3];

		c.m[2][i] += b->m[0][i] * a->m[2][0];
		c.m[2][i] += b->m[1][i] * a->m[2][1];
		c.m[2][i] += b->m[2][i] * a->m[2][2];
		c.m[2][i] += b->m[3][i] * a->m[2][3];

		c.m[3][i] += b->m[0][i] * a->m[3][0];
		c.m[3][i] += b->m[1][i] * a->m[3][1];
		c.m[3][i] += b->m[2][i] * a->m[3][2];
		c.m[3][i] += b->m[3][i] * a->m[3][3];
	}

	return c;
}

static Vec4
Mat4_mul_Vec4(const Mat4 *m, const Vec4 v)
{
	Vec4 r = {0};

	r.x = m->m[0][0] * v.x + m->m[1][0] * v.y + m->m[2][0] * v.z + m->m[3][0] * v.w;
	r.y = m->m[0][1] * v.x + m->m[1][1] * v.y + m->m[2][1] * v.z + m->m[3][1] * v.w;
	r.z = m->m[0][2] * v.x + m->m[1][2] * v.y + m->m[2][2] * v.z + m->m[3][2] * v.w;
	r.w = m->m[0][3] * v.x + m->m[1][3] * v.y + m->m[2][3] * v.z + m->m[3][3] * v.w;
	
	return r;
}

static RGBColor color_to_rgb8(DColor dc) {
	RGBColor color = {
		(((dc & 0xf800) >> 11) * 256) / 32,
		(((dc & 0x7e0) >> 5)   * 256) / 64,
		( (dc & 0x1f)          * 256) / 32,
	};
	return color;
}

static DColor
color_from_rgb8(uint8_t r, uint8_t g, uint8_t b)
{
	DColor color = 0;
	color |= (b * 32) / 256;
	color |= ((g * 64) / 256) << 5;
	color |= ((r * 32) / 256) << 11;
	return color; 
}

static DColor
color_from_rgb(uint8_t r, uint8_t g, uint8_t b)
{
	DColor color = 0;
	color |= b;
	color |= g << 5;
	color |= r << 11;
	return color; 
}

// ===============================================


static inline IVec2 ivec2(int32_t x, int32_t y) {
	return (IVec2){x, y};
}

static inline IVec2 ivec2e(int32_t e) {
	return (IVec2){e, e};
}

static inline Vec2 vec2(float x, float y) {
	return (Vec2){x, y};
}

static inline Vec2 vec2e(float e) {
	return (Vec2){e, e};
}

static inline Vec2 ivec2_to_vec2(IVec2 v) {
	return (Vec2){(float)v.x, (float)v.y};
}

static inline IVec2 vec2_to_ivec2(Vec2 v) {
	return (IVec2){(int32_t)v.x, (int32_t)v.y};
}

// ============================================

static IVec2
IVec2_add(IVec2 a, IVec2 b)
{
	IVec2 result = {a.x + b.x, a.y + b.y};
	return result;
}

static IVec2
IVec2_sub(IVec2 a, IVec2 b)
{
	IVec2 result = {a.x - b.x, a.y - b.y};
	return result;
}

static IVec2
IVec2_mul(IVec2 a, IVec2 b)
{
	IVec2 result = {a.x * b.x, a.y * b.y};
	return result;
}

static IVec2
IVec2_div(IVec2 a, IVec2 b)
{
	IVec2 result = {a.x / b.x, a.y / b.y};
	return result;
}

static Vec2
Vec2_add(Vec2 a, Vec2 b)
{
	Vec2 result = {a.x + b.x, a.y + b.y};
	return result;
}

static Vec2
Vec2_sub(Vec2 a, Vec2 b)
{
	Vec2 result = {a.x - b.x, a.y - b.y};
	return result;
}

static Vec2
Vec2_mul(Vec2 a, Vec2 b)
{
	Vec2 result = {a.x * b.x, a.y * b.y};
	return result;
}

static Vec2
Vec2_div(Vec2 a, Vec2 b)
{
	Vec2 result = {a.x / b.x, a.y / b.y};
	return result;
}

static Vec2
Vec2_floor(Vec2 v)
{
	Vec2 result = {(int)v.x, (int)v.y};
	return result;
}

static Vec2
Vec2_fract(Vec2 v)
{
	Vec2 result = Vec2_sub(v, Vec2_floor(v));
	return result;
}
