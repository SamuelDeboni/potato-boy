#ifndef DISPLAY_H
#define DISPLAY_H

#define SCLK 24
#define SDAT 23
#define DC   25
#define RESET 4
static int pins[4] = {SCLK, SDAT, DC, RESET};

#define BLACK 0x000000
#define RED   0xFF0000
#define GREEN 0x00FF00
#define BLUE  0x0000FF
#define WHITE 0xFFFFFF

// ST7735 commands
#define SWRESET 0x01 // software reset
#define SLPOUT  0x11 // sleep out
#define DISPON  0x29 // display on
#define CASET   0x2A // column address set
#define RASET   0x2b // row address set
#define RAMWR   0x2c // ram write
#define MADCTL  0x36 // axis control
#define COLMOD  0x3A // color mode



static void
pulse_clock()
{
    set_pin(SCLK, 1);
    //nsleep(1);
    set_pin(SCLK, 0);
    //nsleep(1);
}

    static void
write_byte_or_data(uint8_t value, int data)
{
    uint8_t mask = 0x80;
    set_pin(DC, data);
    for (int i = 0; i < 8; i++) {
        set_pin(SDAT, value & mask);
        set_pin(SCLK, 1);
        set_pin(SCLK, 0);
        mask >>= 1;
    }
}

static void write_byte(uint8_t value) {write_byte_or_data(value, 1);}
static void write_cmd(uint8_t value) {write_byte_or_data(value, 0);}

static void 
write_word(uint16_t value)
{
    write_byte(value >> 8);
    write_byte(value & 0xff);
#if 0
    set_pin(DC, 1);
    uint16_t mask = 0x80;
    for (int i = 0; i < 8; i++) {
        set_pin(SDAT, value & mask);
        set_pin(SCLK, 1);
        set_pin(SCLK, 0);
        mask >>= 1;
    }

    mask = 0x8000;
    for (int i = 0; i < 8; i++) {
        set_pin(SDAT, value & mask);
        set_pin(SCLK, 1);
        set_pin(SCLK, 0);
        mask >>= 1;
    }
#endif
}

static void
write_array(uint8_t *arr, int len)
{
    for (int i = 0; i < len; i++) {
        write_byte(arr[i]);
    }
}


static void
write888(uint32_t value, int reps)
{
    uint8_t red   = value >> 16;
    uint8_t green = (value >> 8) & 0xFF;
    uint8_t blue  = value & 0xFF;
    uint8_t rgb[3]  = {red, green, blue};
    for (int i = 0; i < reps; i++) {
        write_array(rgb, 3);
    }
}

static void
init_display()
{
    for (int i = 0; i < 4; i++) {
        INP_GPIO(pins[i]);
        OUT_GPIO(pins[i]);
    }

    set_pin(RESET, 0);
    usleep(200000);
    set_pin(RESET, 1);
    usleep(200000);
    write_cmd(SWRESET);
    usleep(200000);
    write_cmd(SLPOUT);
    usleep(200000);
    write_cmd(DISPON);
    usleep(200000);
    write_cmd(COLMOD);
    write_byte(0x05);
}

static void
set_addr_window(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1)
{
    write_cmd(CASET);
    write_word(x0);
    write_word(x1);
    write_cmd(RASET);
    write_word(y0);
    write_word(y1);
}

#endif