#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "../input.h"
#include "../dmath.h"
#include "../draw.h"


#define SEG_SIZE 4

typedef struct {
	int segment_count;
	IVec2 segments[1300];
	IVec2 dir;
} Snake;

static Snake snake;

static IVec2 snack[64];
static int snack_count;
static int snack_counter;

static int snake_counter;
static int snake_has_moved;

void
snake_start()
{
	snake = (Snake){0};
	snake.segment_count = 3;
	snake.dir = ivec2(0, 1);

	snake.segments[0] = ivec2(16, 20);
	snake.segments[1] = ivec2(16, 19);
	snake.segments[2] = ivec2(16, 18);

	snack_count = 1;
	snack[0] = ivec2(rand() % 32, rand() % 38);

	snake_counter = 0;
}


void
snake_update_and_render()
{
	if (!snake_has_moved) {
		if (pressed[B_RIGHT] && snake.dir.y != 0) {
			snake.dir = ivec2(1, 0);
			snake_has_moved = 1;
		} else if (pressed[B_LEFT]  && snake.dir.y != 0) {
			snake.dir = ivec2(-1, 0);
			snake_has_moved = 1;
		} else if (pressed[B_UP]    && snake.dir.x != 0) {
			snake.dir = ivec2(0, 1);
			snake_has_moved = 1;
		} else if (pressed[B_DOWN]  && snake.dir.x != 0) {
			snake.dir = ivec2(0, -1);
			snake_has_moved = 1;
		}
	}

	snake_counter++;
	if (snake_counter > 5) {
		snake_counter = 0;
		snake_has_moved = 0;

		snake_counter++;

		IVec2 prev_pos = snake.segments[0];
		snake.segments[0] = IVec2_add(snake.segments[0], snake.dir);

		if (snake.segments[0].x > 31) snake.segments[0].x = 0;
		if (snake.segments[0].x < 0)  snake.segments[0].x = 31;
		if (snake.segments[0].y > 37) snake.segments[0].y = 0;
		if (snake.segments[0].y < 0)  snake.segments[0].y = 37;

		int snack_index = -1;
		for (int i = 0; i < snack_count; i++) {
			if (snake.segments[0].x == snack[i].x &&
				snake.segments[0].y == snack[i].y)
			{
				snack_index = i;
			}
		}

		for (int i = 2; i < snake.segment_count; i++) {
			if (snake.segments[0].x == snake.segments[i - 1].x &&
				snake.segments[0].y == snake.segments[i - 1].y)
			{
				snake.segment_count = i;
				break;
			}
		}

		if (snack_index != -1) {
			snack[snack_index] = snack[--snack_count];

			snake.segment_count++;
			for (int i = snake.segment_count - 1; i > 0; i--) {
				snake.segments[i] = snake.segments[i - 1];
			}

			if (snack_count < 64) {
				snack[snack_count] = ivec2(rand() % 32, rand() % 38);
				snack_count++;
			}
		}

		for (int i = 1; i < snake.segment_count; i++) {
			IVec2 tmp = snake.segments[i];
			snake.segments[i] = prev_pos;
			prev_pos = tmp;
		}
	}

	if (snack_counter == 32) {
		snack_counter = 0;
		if (snack_count < 64) {
			snack[snack_count] = ivec2(rand() % 32, rand() % 38);
			snack_count++;
		}	
	}

	// draw background
	for (int y = 0; y < 19; y++) {
		for (int x = 0; x < 16; x++) {
			DColor color = color_from_rgb8(32, 120, 20);
			if (y%2 && x%2 || y%2 == 0 && x%2 == 0) {
				color = color_from_rgb8(32, 100, 20); 
			}
			draw_solid_rect((Rect){x*8, y*8, 8, 8}, color);
		}
	}

	// draw snake
	for (int i = 0; i < snake.segment_count; i++) {
		IVec2 seg = snake.segments[i];
		Rect rect = {seg.x * 4, seg.y * 4, 4, 4};
		DColor color = color_from_rgb8(255 - i * (255 / snake.segment_count), 90, 50);
		draw_solid_rect(rect, color);
	}

	for (int i = 0; i < snack_count; i++) {
		IVec2 s = snack[i];
		Rect rect = {s.x * 4, s.y * 4, 4, 4};
		draw_solid_rect(rect, color_from_rgb8(50, 50, 255));
	}

	char buf[64];
	int t_len = sprintf(buf, "SCORE: %d", snake.segment_count - 3);
	draw_text_len(buf, t_len, vec2(0, 152), 0xFFFF);
}