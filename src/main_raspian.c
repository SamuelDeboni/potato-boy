#include <stdint.h>

#include "gpio.h"
#include "util.h"
#include "display.h"

#include "dmath.h"
#include "draw.h"
#include "draw.c"

#define SDF_MEM_IMPLEMENTATION
#include <sdf/sdf_mem.h>
#define SDF_TGA_IMPLEMENTATION
#include <sdf/sdf_image.h>

#define SDF_ALLOCATORS
#include <sdf/plataform/sdf_plataform.h>
#include <sdf/plataform/sdf_plataform_linux.c>


#include <stdio.h>
 
// ==== gamepad ====

static int input_pins[7] = {17, 20, 16, 26, 19, 13, 12};

static void
init_input()
{
    for (int i = 0; i < 7; i++) {
        INP_GPIO(input_pins[i]);
    }
}

#include "input.h"

int pressed[9];
int prev_pressed[9];

static void
read_input()
{
	for (int i = 0; i < 9; i++) {
		prev_pressed[i] = pressed[i];
		pressed[i] = 0;
	}
	//printf("\n");

	for (int p = 0; p < 7; p++) {
	    INP_GPIO(input_pins[p]);
	    OUT_GPIO(input_pins[p]);
	    set_pin(input_pins[p], 1)            

	    for (int p2 = 0; p2 < 7; p2++) {
	    	if (p2 == p) continue;

	    	if (GET_GPIO(input_pins[p2])) {
	    		if (p == 3 && p2 == 2) pressed[B_ACTION] = 1;
	    		if (p == 3 && p2 == 4) pressed[B_RIGHT] = 1;
	    		if (p == 3 && p2 == 5) pressed[B_ONOFF] = 1;
	    		if (p == 3 && p2 == 6) pressed[B_RESET] = 1;

	    		if (p == 0 && p2 == 3) pressed[B_UP] = 1;
	    		if (p == 0 && p2 == 5) pressed[B_DOWN] = 1;
	    		if (p == 0 && p2 == 1 || p == 1 && p2 == 0) {
	    			//printf("yay\n");
	    			pressed[B_START] = 1;
	    		}

	    		if (p == 4 && p2 == 5) pressed[B_LEFT] = 1;
	    		if (p == 1 && p2 == 2) pressed[B_SOUND] = 1;
	    	}
	    }

	    set_pin(input_pins[p], 0);
	    INP_GPIO(input_pins[p]);
	}
}

static void
swap()
{

    set_addr_window(0, 0, 127, 159);
    write_cmd(RAMWR);

    for (int y = S_HEIGHT - 1; y >= 0; y--) {
    	for (int x = 0; x < S_WIDTH; x++) {
    		int j = y * S_WIDTH + x;
        	write_word(screen[j]);
	        screen[j] = 0;
    	}
    }

}

static int running = 1;
static int restart = 0;
#include "games.c"

SdfArena g_arena;
SdfArena f_arena;

int
main()
{
	RESTART:

	printf("Init IO\n");
    init_io();
    usleep(10000);

	printf("Init Display\n");
    init_display();
    usleep(10000);

	printf("Init Input\n");
    init_input();
    usleep(10000);

    g_arena = sdf_arena_create(sdf_alloc(MEGA(128)), MEGA(128));
    f_arena = sdf_arena_create(sdf_alloc(MEGA(128)), MEGA(128));

    start_games();


    while (running) {	
    	f_arena.offset = 0;

   		clock_t t = clock();

        read_input();

		update_games();
		if (restart) {
			restart = 0;
			goto RESTART;
		}

        swap();

	    t = clock() - t;
	    double time_taken = ((double)t)/CLOCKS_PER_SEC;
	    double sleep_time = time_taken > 0.0333 ? 0.0 : 0.0333 - time_taken;
	    usleep(sleep_time * 1000 * 1000);
	    //printf("%lf + %lf = %lf\n", sleep_time, time_taken, sleep_time + time_taken);

        if (!pressed[B_SOUND] && prev_pressed[B_SOUND]) {
        	selected_game = 0;
        }
    }

    draw_solid_rect((Rect){0, 0, 128, 160}, 0);
    swap();
    return 0;
}
