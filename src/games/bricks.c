#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "../input.h"
#include "../dmath.h"
#include "../draw.h"

static uint16_t bricks_list[28][16] = {
	// O
	{0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0},

	// I
	{0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0},
	{0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0},
	{0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},

	// T
	{0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0},
	{0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
	{0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},

	// S
	{0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0},
	{1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
	{0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},

	// Z
	{0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0},
	{0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},

	// J
	{0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0},
	{0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
	{1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},

	// L
	{0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
	{0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
};

static uint16_t bricks_color[7] = {1, 2, 3, 4, 5, 6, 7};


typedef struct {
	uint16_t canvas[20][10];
	int next_brick;
	int current_brick;

	int brick_rotation;
	int next_brick_rotation;
	IVec2 brick_pos;
	IVec2 next_brick_pos;

	int input_counter;
	int counter;
	int to_canvas;

	int score;
	int level;
	int level_counter;
	int game_over;
} BricksState;

static BricksState brick_state;

void
bricks_start()
{
	load_spritesheet("assets/bricks_spritesheet.tga");

	for (int y = 0; y < 20; y++) {
		for (int x = 0; x < 10; x++) {
			brick_state.canvas[y][x] = 0;
		}
	}

	brick_state.next_brick     = rand() % 7;
	brick_state.current_brick  = rand() % 7;
	brick_state.brick_rotation = 0;
	brick_state.next_brick_rotation = 0;
	brick_state.brick_pos      = ivec2(3, 20);
	brick_state.next_brick_pos = ivec2(3, 20);
	brick_state.counter   = 0;
	brick_state.to_canvas = 0;
	brick_state.input_counter = 0;
	brick_state.score = 0;
	brick_state.level = 1;
	brick_state.level_counter = 0;
	brick_state.game_over = 0;
}

void
bricks_update_and_render()
{

	if (brick_state.game_over) {
		if (pressed[B_RESET] && !prev_pressed[B_RESET]) {
			bricks_start();
		}
		goto BRICKS_SKIP_TO_DRAW;
	}

	if (pressed[B_RIGHT] && !prev_pressed[B_RIGHT]) {
		brick_state.next_brick_pos.x++;
		brick_state.input_counter = 5;
	} 
	if (pressed[B_LEFT] && !prev_pressed[B_LEFT]) {
		brick_state.next_brick_pos.x--;
		brick_state.input_counter = 5;
	} 
	if (brick_state.input_counter == 0) {
		if (pressed[B_RIGHT]) {
			brick_state.next_brick_pos.x++;
			brick_state.input_counter = 2;
		} 
		if (pressed[B_LEFT]) {
			brick_state.next_brick_pos.x--;
			brick_state.input_counter = 2;
		} 
	} else {
		brick_state.input_counter--;
	}
	if (pressed[B_ACTION] && !prev_pressed[B_ACTION]) {
		brick_state.next_brick_rotation = (brick_state.next_brick_rotation + 1) % 4;
	} 

	brick_state.counter++;

	if (brick_state.counter >= (pressed[B_DOWN] ? 1.0f : 15.0 - brick_state.level)) {
		brick_state.counter = 0;

		if (!brick_state.to_canvas) {
			brick_state.next_brick_pos.y--;
		} else {
			brick_state.to_canvas = 0;


			for (int yi = 0; yi < 4; yi++) { 
				int y = yi + brick_state.next_brick_pos.y;
				for (int xi = 0; xi < 4; xi++) { 
					int x = xi + brick_state.next_brick_pos.x;
					if (bricks_list[brick_state.next_brick_rotation + brick_state.current_brick * 4][xi + yi * 4]) {
						brick_state.canvas[y][x] = bricks_color[brick_state.current_brick];
					}
				}
			}

			brick_state.current_brick = brick_state.next_brick;
			brick_state.next_brick     = rand() % 7;
			brick_state.brick_rotation = 0;
			brick_state.next_brick_rotation = 0;
			brick_state.brick_pos      = ivec2(3, 20);
			brick_state.next_brick_pos = ivec2(3, 20);
		}

		// clear rows
		int rows_cleared = 0;
		for (int y = 0; y < 20;) {
			int row_full = 1;
			for (int x = 0; x < 10; x++) {
				if (!brick_state.canvas[y][x]) {
					row_full = 0;
					break;
				}
			}

			if (row_full) {
				for (int y2 = y; y2 < 19; y2++) {
					for (int x = 0; x < 10; x++) {
						brick_state.canvas[y2][x] = brick_state.canvas[y2+1][x];
					}
				}
				rows_cleared++;
			} else {
				y++;
			}

			if (!row_full && rows_cleared > 0) {
				rows_cleared = 1;
			}
		}

		if (rows_cleared) {
			brick_state.score += (400 + rows_cleared * 800) * brick_state.level;

			brick_state.level_counter++;
			if (brick_state.level_counter > 5) {
				brick_state.level++;
				if (brick_state.level > 15) brick_state.level = 15;
				brick_state.level_counter = 0;
			}
		}
	}

	// Move brick
	{
		int apply_movement = 1;

		for (int yi = 0; yi < 4; yi++) { 
			int y = yi + brick_state.next_brick_pos.y;
			if (y >= 20) continue;
			for (int xi = 0; xi < 4; xi++) { 
				int x = xi + brick_state.next_brick_pos.x;

				uint16_t b = bricks_list[brick_state.next_brick_rotation + brick_state.current_brick * 4][xi + yi * 4];
				if (!b) continue;

				if (x >= 10 || x < 0 || y < 0 || brick_state.canvas[y][x]) {
					apply_movement = 0;
				}
			}
		}

		if (apply_movement) {
			brick_state.brick_pos = brick_state.next_brick_pos;
			brick_state.brick_rotation = brick_state.next_brick_rotation;
			brick_state.to_canvas = 0;
		} else {
			brick_state.next_brick_pos = brick_state.brick_pos;
			brick_state.next_brick_rotation = brick_state.brick_rotation;
		}

		for (int yi = 0; yi < 4; yi++) { 
			int y = yi + brick_state.brick_pos.y;
			if (y > 20) continue;
			for (int xi = 0; xi < 4; xi++) { 
				int x = xi + brick_state.brick_pos.x;
				uint16_t b = bricks_list[brick_state.brick_rotation + brick_state.current_brick * 4][xi + yi * 4];
				if (!b) continue;

				if (y == 0 || brick_state.canvas[y-1][x]) {
					brick_state.to_canvas = 1;

					if (y >= 19) {
						brick_state.game_over = 1;	
					}
				}
			}
		}
	}

	BRICKS_SKIP_TO_DRAW:

	if (brick_state.game_over) {
		draw_solid_rect((Rect){0, 0, 80, 160}, color_from_rgb8(50, 0, 0));
	}

	// draw canvas
	for (int y = 0; y < 20; y++) {
		for (int x = 0; x < 10; x++) {
			if (!brick_state.canvas[y][x]) continue;
			draw_sprite8(vec2(x * 8, y * 8), brick_state.canvas[y][x]);
		}
	}


	// draw current brick
	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 4; x++) {
			if (!bricks_list[brick_state.current_brick * 4 + brick_state.brick_rotation][x + y * 4])
				continue;

			Vec2 pos = {
				x * 8 + brick_state.brick_pos.x * 8,
				y * 8 + brick_state.brick_pos.y * 8
			};
			draw_sprite8(pos, bricks_color[brick_state.current_brick]);
		}
	}

	draw_solid_rect((Rect){80, 0, 48, S_HEIGHT},
		color_from_rgb8(75, 75, 75));
	draw_solid_rect((Rect){81, 1, 46, S_HEIGHT - 2},
		color_from_rgb8(50, 50, 50));
	draw_solid_rect((Rect){11 * 8, 13 * 8, 32, 32}, 0);
	draw_solid_rect((Rect){10 * 8, 18 * 8, 32 + 16, 16}, color_from_rgb8(50, 80, 80));

	draw_text("POTATO", vec2(10 * 8, 19 * 8 - 1), 0xffff);
	draw_text("BRICKS", vec2(10 * 8, 18 * 8 - 1), 0xffff);

	char buff[16];
	draw_text("LEVEL", vec2(10 * 8 + 4, 11 * 8 - 1), 0xffff);
	{
		int len = sprintf(buff, "%0.2d", brick_state.level);
		draw_text_len(buff, len, vec2(12 * 8, 10 * 8 - 4), 0xffff);
	}
	draw_text("SCORE", vec2(10 * 8 + 3, 8 * 8 - 1), 0xffff);
	{
		int len = sprintf(buff, "%0.5d", brick_state.score);
		draw_text_len(buff, len, vec2(10 * 8 + 4, 7 * 8 - 4), 0xffff);
	}

	// draw next brick
	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 4; x++) {
			if (!bricks_list[brick_state.next_brick * 4][x + y * 4])
				continue;
			draw_sprite8(vec2(x * 8 + 11 * 8, y * 8 + 13 * 8), bricks_color[brick_state.next_brick]);
		}
	}
}