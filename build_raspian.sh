#!/bin/bash

echo "building"
GAMES='src/games/3d_demo.c src/games/maze.c src/games/snake.c src/games/breakout.c src/games/bricks.c src/games/shapes.c'
clang src/main.c $GAMES -o pb -O2 -Iinclude -lm

if [[ $1 = 'run' ]] ; then
	echo "running"
	sudo ./pb
fi
