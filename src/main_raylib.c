#include <stdint.h>
#include <stdio.h>

#include "dmath.h"
#include "util.h"
#include "draw.h"
#include "draw.c"
#include "../include/raylib.h"

#define SDF_MEM_IMPLEMENTATION
#include <sdf/sdf_mem.h>
#define SDF_TGA_IMPLEMENTATION
#include <sdf/sdf_image.h>

#define SDF_ALLOCATORS
#include <sdf/plataform/sdf_plataform.h>
#include <sdf/plataform/sdf_plataform_linux.c>

#include "input.h" 

int pressed[9];
int prev_pressed[9];

static void
read_input()
{
	for (int i = 0; i < 9; i++) {
		prev_pressed[i] = pressed[i];
		pressed[i] = 0;
	}

	if (IsKeyDown(KEY_W)) pressed[B_UP]    = 1;	
	if (IsKeyDown(KEY_A)) pressed[B_LEFT]  = 1;	
	if (IsKeyDown(KEY_S)) pressed[B_DOWN]  = 1;	
	if (IsKeyDown(KEY_D)) pressed[B_RIGHT] = 1;	

	if (IsKeyDown(KEY_SPACE)) pressed[B_ACTION] = 1;
	if (IsKeyDown(KEY_R)) pressed[B_RESET] = 1;
	if (IsKeyDown(KEY_F)) pressed[B_ONOFF] = 1;
	if (IsKeyDown(KEY_V)) pressed[B_SOUND] = 1;
}

static void
swap()
{
	BeginDrawing();

	for (int y = 0; y < S_HEIGHT; y++) {
		for (int x = 0; x < S_WIDTH; x++) {
			DColor dc = screen[x + y * S_WIDTH];
			Color color = (Color){
				(((dc & 0xf800) >> 11) * 256) / 32,
				(((dc & 0x7e0) >> 5) * 256) / 64,
				((dc & 0x1f) * 256) / 32,
				255
			};

			DrawRectangle(x * 4, 636 - y * 4, 4, 4, color);
			screen[x + y * S_WIDTH] = 0;
		}
	}

	DrawFPS(490, 0);

    EndDrawing();
}

static int running = 1;
static int restart = 0;

#include "games.c"

SdfArena g_arena;
SdfArena f_arena;

int
main()
{
    InitWindow(512, 640, "Potato Boy");
    SetTargetFPS(30);

    g_arena = sdf_arena_create(sdf_alloc(MEGA(128)), MEGA(128));
    f_arena = sdf_arena_create(sdf_alloc(MEGA(128)), MEGA(128));

    start_games();

	int game_selector = 1;

    while(!WindowShouldClose()) {
    	f_arena.offset = 0;

        read_input();

		update_games();

        swap();

        if (!pressed[B_SOUND] && prev_pressed[B_SOUND]) {
        	selected_game = 0;
        }
    }

    return 0;
}
