#ifndef GPIO_H
#define GPIO_H

#include <stdlib.h>
#include <unistd.h>

#define BCM2708_PERI_BASE	0x20000000
#define GPIO_BASE		(BCM2708_PERI_BASE + 0x200000)

#include <fcntl.h>
#include <sys/mman.h>

#define PAGE_SIZE  (4*1024)
#define BLOCK_SIZE (4*1024)

int mem_fd;
void *gpio_map;

volatile unsigned *gpio;

#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) *(gpio+(((g)/10))) |= (((a)<=3?(a)+4:(a)==4?3:2)<<(((g)%10)*3))

#define GPIO_SET *(gpio+7)  // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10) // clears bits which are 1 ignores bits which are 0

#define GET_GPIO(g) (*(gpio+13)&(1<<g)) // 0 if LOW, (1<<g) if HIGH

#define GPIO_PULL *(gpio+37)     // Pull up/pull down
#define GPIO_PULLCLK0 *(gpio+38) // Pull up/pull down clock


#if 0
    inline void
set_pin(int pin_number, int value)
{
    if (value == 0) {
        GPIO_CLR = 1 << pin_number;
    } else {
        GPIO_SET = 1 << pin_number;
    }
}
#else
#define set_pin(p, v) if(v) GPIO_SET = 1 << p; else GPIO_CLR = 1 << p;
#endif

static void
init_io()
{
    if ((mem_fd = open("/dev/mem", O_RDWR | O_SYNC)) < 0) {
        exit(-1);
    }

    gpio_map = mmap(
            NULL,
            BLOCK_SIZE,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            mem_fd,
            GPIO_BASE
            );

    close(mem_fd);

    if (gpio_map == MAP_FAILED) {
        exit(-1);
    }

    gpio = (volatile unsigned *)gpio_map;

}

#endif