#include "draw.h"

#include <sdf/sdf_mem.h>
#include <sdf/sdf_image.h>

uint16_t screen[S_WIDTH * S_HEIGHT];

static DImage spritesheet;
extern SdfArena g_arena;
extern SdfArena f_arena;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

static void *
read_file(const char *path, SdfArena *arena, size_t *out_size)
{
    size_t size; 

    FILE *fn = fopen(path, "rb");
    fseek(fn, 0, SEEK_END);
    size = ftell(fn);
    fseek(fn, 0, SEEK_SET);
    
    void *b = sdf_arena_alloc(arena, size);
    fread(b, 1, size, fn);
    
    fclose(fn);

    if (out_size) *out_size = size;
    
    return b;
}

int load_spritesheet(char *path) {
	size_t data_size = 0;
	void *tga_data = read_file(path, &f_arena, &data_size);

	SdfImage sdf_image = sdf_image_from_tga_data(tga_data, data_size, 0);

	uint8_t *image_buffer = sdf_arena_alloc(&f_arena, sdf_image.w * sdf_image.h * 4);
	sdf_image = sdf_image_from_tga_data(tga_data, data_size, image_buffer);

	spritesheet.w = sdf_image.w;
	spritesheet.h = sdf_image.h;
	spritesheet.data = sdf_arena_alloc(&g_arena, spritesheet.w * spritesheet.h); 
	for (int i = 0; i < sdf_image.w * sdf_image.h; i++) {
		uint8_t r = sdf_image.data[i * 4];
		uint8_t g = sdf_image.data[i * 4 + 1];
		uint8_t b = sdf_image.data[i * 4 + 2];
		uint8_t a = sdf_image.data[i * 4 + 3];
		if (a == 255) {
			spritesheet.data[i] = color_from_rgb8(r, g, b);
		} else {
			spritesheet.data[i] = 0xF81F; 
		}
	}

	return 0;
}

void
draw_solid_rect(Rect rect, DColor color)
{
	int x0 = rect.x;
	int x1 = rect.x + rect.w;
	int y0 = rect.y;
	int y1 = rect.y + rect.h;

	// rect out of screen
	if (x1 < 0 || y1 < 0 || x0 >= S_WIDTH || y0 >= S_HEIGHT) {
		return;
	}

	// CLAMP to edge
	x0 = x0 >= 0 ? x0 : 0;
	y0 = y0 >= 0 ? y0 : 0;
	x1 = x1 < S_WIDTH  ? x1 : S_WIDTH;
	y1 = y1 < S_HEIGHT ? y1 : S_HEIGHT;

	for (int y = y0; y < y1; y++) {
		for (int x = x0; x < x1; x++) {
			screen[x + y * S_WIDTH] = color;
		}
	}
}

void
draw_sprite8(Vec2 pos, int idx)
{
	int tx = SPRITE8_X(spritesheet, idx); 
	int ty = SPRITE8_Y(spritesheet, idx); 

	int v = ty + 7;
	for (int y = pos.y; y < pos.y + 8; y++, v--) {
		if (y < 0 || y >= S_HEIGHT) continue;

		int u = tx;
		for (int x = pos.x; x < pos.x + 8; x++, u++) {
			if (x < 0 || x >= S_WIDTH) continue;

			DColor c =
				spritesheet.data[u + v * spritesheet.w];
			if (c != 0xF81F)
				SCREEN_PIXEL(x, y) = c;
		}
	} 
}

void
draw_sprite8_scale(Vec2 pos, Vec2 scale, int idx)
{
	int tx = SPRITE8_X(spritesheet, idx); 
	int ty = SPRITE8_Y(spritesheet, idx); 
	ty += 7;

	int x0 = pos.x >= 0 ? (int)pos.x : 0;
	int y0 = pos.y >= 0 ? (int)pos.y : 0;

	for (int y = 0; y < 8 * scale.y; y++) {
		if ((y + y0) >= S_HEIGHT) continue;

		int tyd = (y / (8.0f * scale.y)) * 8;
		int v = ty - tyd;

		for (int x = 0; x < 8 * scale.x; x++) {
			if ((x + x0) >= S_WIDTH) continue;

			int txd = (x / (8.0f * scale.x)) * 8;
			int u = tx + txd;

			DColor c =
				spritesheet.data[u + v * spritesheet.w];

			if (c != 0xF81F)
				SCREEN_PIXEL(x + x0, y + y0) = c;
		}
	}
}

DImage
get_sprite8(int idx, uint16_t *mem)
{
	int tx = idx % (spritesheet.w / 8);
	int ty = idx / (spritesheet.h / 8);
	tx *= 8;
	ty *= 8;

	int v = ty + 7;
	for (int y = 0; y < 8; y++, v--) {
		int u = tx;
		for (int x = 0; x < 8; x++, u++) {

			DColor c =
				spritesheet.data[u + v * spritesheet.w];

			mem[x + y * 8] = c;
		}
	} 

	return (DImage){8, 8, mem};
}

void
draw_line(Vec2 point_0, Vec2 point_1, DColor color) {

    // Verify if the line is outside of the screen
    if (point_0.x < 0 && point_1.x < 0 ||
        point_0.y < 0 && point_1.y < 0 ||
        point_0.x >= S_WIDTH  && point_1.x >= S_WIDTH ||
        point_0.y >= S_HEIGHT && point_1.y >= S_HEIGHT)
    {
        return;
    }
   
    #define swap_if_greater(a, b, c) \
        if (a.array[c] > b.array[c]) { \
            Vec2 tmp = a;   \
            a = b;     \
            b = tmp;    \
        } \
    
    Vec2 p0 = point_0;
    Vec2 p1 = point_1; 
    
    // NOTE(samuel): Clamp the points
    {
        swap_if_greater(p0, p1, 0);
        
        float a = (p1.y - p0.y) / (p1.x - p0.x);
        if (p0.x < 0) {
            float x_delta = -p0.x;
            p0.x = 0;
            p0.y += x_delta * a;
        }
        
        if (p1.x >= S_WIDTH) {
            float x_delta = S_WIDTH - p1.x;
            p1.x = S_WIDTH - 1;
            p1.y += x_delta * a;
        }
        
        swap_if_greater(p0, p1, 1);
        
        a = (p1.x - p0.x) / (p1.y - p0.y);
        if (p0.y < 0) {
            float y_delta = -p0.y;
            p0.x += y_delta * a;
            p0.y = 0;
        }
        
        if (p1.y >= S_HEIGHT) {
            float y_delta = S_HEIGHT - p1.y;
            p1.x += y_delta * a;
            p1.y = S_HEIGHT - 1;
        }
    }

    // NOTE(samuel): Drawing loop  
    if (fabs(p1.x - p0.x) > fabs(p1.y - p0.y)) {
        swap_if_greater(p0, p1, 0);
        
        float delta = (p1.y - p0.y) / (p1.x - p0.x);
        float y = p0.y;
        
        for (int x = p0.x; x <= (int)p1.x; x += 1) {
            SCREEN_PIXEL((int)x, (int)y) = color;
            y += delta;
        }
    } else {
        swap_if_greater(p0, p1, 1);
        
        float delta = (p1.x - p0.x) / (p1.y - p0.y);
        float x = p0.x;
        
        for (int y = p0.y; y <= (int)p1.y; y += 1) {
            SCREEN_PIXEL((int)x, (int)y) = color;
            x += delta;
        }
    }

    #undef swap_if_greater
}

void
draw_triangle(Vec2 p0, Vec2 p1, Vec2 p2, DColor color)
{
	draw_line(p0, p1, color);
	draw_line(p1, p2, color);
	draw_line(p2, p0, color);
}

void
draw_solid_triangle(Vec2 p0, Vec2 p1, Vec2 p2, DColor color)
{
    Vec2 v0 = Vec2_sub(p1, p0);
    Vec2 v1 = Vec2_sub(p2, p1);
    Vec2 v2 = Vec2_sub(p0, p2);
    
    float x0 = MIN(p0.x, MIN(p1.x, p2.x));
    float y0 = MIN(p0.y, MIN(p1.y, p2.y));
    float x1 = MAX(p0.x, MAX(p1.x, p2.x));
    float y1 = MAX(p0.y, MAX(p1.y, p2.y));
    
    x0 = CLAMP(x0, 0, S_WIDTH);
    y0 = CLAMP(y0, 0, S_HEIGHT);
    x1 = CLAMP(x1, 0, S_WIDTH);
    y1 = CLAMP(y1, 0, S_HEIGHT);
    
    uint16_t *sp = screen;
    sp += (int)y0 * S_WIDTH + (int)x0;
    
    for (int y_i = y0; y_i < y1; y_i += 1) {
        uint16_t *sp2 = sp;
        
        float vy0 = y_i - p0.y;
        float vy1 = y_i - p1.y;
        float vy2 = y_i - p2.y;
        
        float y_v0 = -vy0 * v0.x;
        float y_v1 = -vy1 * v1.x;
        float y_v2 = -vy2 * v2.x;
        
        for (int x_i = x0; x_i < x1; x_i += 1) {
            float vx0 = x_i - p0.x;
            float vx1 = x_i - p1.x;
            float vx2 = x_i - p2.x;
            
            float dot_0 = vx0 * v0.y + y_v0;
            float dot_1 = vx1 * v1.y + y_v1;
            float dot_2 = vx2 * v2.y + y_v2;
            
            if (dot_0 >= 0 && dot_1 >= 0 && dot_2 >= 0) {
            	*sp2 = color;
         	}
            
            sp2++;
        }
        
        sp += S_WIDTH;
    }	
}
	
static Draw3DContext ctx3d;

void
start_3d_draw()
{
	ctx3d.vbuffer_len = 0;
}

void
push_vertex(Vec3 vertex, Vec3 color) {
	if (ctx3d.vbuffer_len >= DRAW_V_BUFFER_SIZE) return;
	ctx3d.vpos[ctx3d.vbuffer_len] = vertex;
	ctx3d.vcol[ctx3d.vbuffer_len] = color;
	ctx3d.vbuffer_len++;
}

void
set_matrix(Mat4 mat)
{
	ctx3d.mat = mat;
}

void
set_projection(float near, float far, float fov)
{
	ctx3d.near = near;
	ctx3d.far  = far;
	ctx3d.s    = tan(fov * (3.1415926535/180.0f)) * near;
}

void
end_3d_draw()
{
	for (int i = 0; i < ctx3d.vbuffer_len; i++) {
		Vec4 v = {0, 0, 0, 1};
		v.xyz = ctx3d.vpos[i];	
		v = Mat4_mul_Vec4(&ctx3d.mat, v);
		ctx3d.vpos[i] = v.xyz;
	}

	int tcount = ctx3d.vbuffer_len / 3;
	for (int i = 0; i < tcount; i++) {
		Vec3 p0 = ctx3d.vpos[i * 3];
		Vec3 p1 = ctx3d.vpos[i * 3 + 1];
		Vec3 p2 = ctx3d.vpos[i * 3 + 2];
		Vec3 col = ctx3d.vcol[i * 3];

		#if 1
		p0.x = (p0.x * ctx3d.near) / (p0.y * ctx3d.s);
		p1.x = (p1.x * ctx3d.near) / (p1.y * ctx3d.s);
		p2.x = (p2.x * ctx3d.near) / (p2.y * ctx3d.s);

		p0.z = (p0.z * ctx3d.near) / (p0.y * ctx3d.s);
		p1.z = (p1.z * ctx3d.near) / (p1.y * ctx3d.s);
		p2.z = (p2.z * ctx3d.near) / (p2.y * ctx3d.s);

		p0.y = (p0.y - ctx3d.near) / (ctx3d.far - ctx3d.near);
		p1.y = (p1.y - ctx3d.near) / (ctx3d.far - ctx3d.near);
		p2.y = (p2.y - ctx3d.near) / (ctx3d.far - ctx3d.near);
		#endif

		draw_solid_triangle(
			Vec2_add(Vec2_mul(vec2(p0.x, p0.z), vec2e(64)), vec2e(64)),
			Vec2_add(Vec2_mul(vec2(p1.x, p1.z), vec2e(64)), vec2e(64)),
			Vec2_add(Vec2_mul(vec2(p2.x, p2.z), vec2e(64)), vec2e(64)),
			color_from_rgb8(col.r * 255, col.g * 255, col.b * 255)
		);
	}
}

static uint64_t draw_font[128] = {
	0x7E7E7E7E7E7E0000,	/* NUL */
	0x7E7E7E7E7E7E0000,	/* SOH */
	0x7E7E7E7E7E7E0000,	/* STX */
	0x7E7E7E7E7E7E0000,	/* ETX */
	0x7E7E7E7E7E7E0000,	/* EOT */
	0x7E7E7E7E7E7E0000,	/* ENQ */
	0x7E7E7E7E7E7E0000,	/* ACK */
	0x7E7E7E7E7E7E0000,	/* BEL */
	0x7E7E7E7E7E7E0000,	/* BS */
	0x0,			/* TAB */
	0x7E7E7E7E7E7E0000,	/* LF */
	0x7E7E7E7E7E7E0000,	/* VT */
	0x7E7E7E7E7E7E0000,	/* FF */
	0x7E7E7E7E7E7E0000,	/* CR */
	0x7E7E7E7E7E7E0000,	/* SO */
	0x7E7E7E7E7E7E0000,	/* SI */
	0x7E7E7E7E7E7E0000,	/* DLE */
	0x7E7E7E7E7E7E0000,	/* DC1 */
	0x7E7E7E7E7E7E0000,	/* DC2 */
	0x7E7E7E7E7E7E0000,	/* DC3 */
	0x7E7E7E7E7E7E0000,	/* DC4 */
	0x7E7E7E7E7E7E0000,	/* NAK */
	0x7E7E7E7E7E7E0000,	/* SYN */
	0x7E7E7E7E7E7E0000,	/* ETB */
	0x7E7E7E7E7E7E0000,	/* CAN */
	0x7E7E7E7E7E7E0000,	/* EM */
	0x7E7E7E7E7E7E0000,	/* SUB */
	0x7E7E7E7E7E7E0000,	/* ESC */
	0x7E7E7E7E7E7E0000,	/* FS */
	0x7E7E7E7E7E7E0000,	/* GS */
	0x7E7E7E7E7E7E0000,	/* RS */
	0x7E7E7E7E7E7E0000,	/* US */
	0x0,			/* (space) */
	0x808080800080000,	/* ! */
	0x2828000000000000,	/* " */
	0x287C287C280000,	/* # */
	0x81E281C0A3C0800,	/* $ */
	0x6094681629060000,	/* % */
	0x1C20201926190000,	/* & */
	0x808000000000000,	/* ' */
	0x810202010080000,	/* ( */
	0x1008040408100000,	/* ) */
	0x2A1C3E1C2A000000,	/* * */
	0x8083E08080000,	/* + */
	0x81000,		/* , */
	0x3C00000000,		/* - */
	0x80000,		/* . */
	0x204081020400000,	/* / */
	0x1824424224180000,	/* 0 */
	0x8180808081C0000,	/* 1 */
	0x3C420418207E0000,	/* 2 */
	0x3C420418423C0000,	/* 3 */
	0x81828487C080000,	/* 4 */
	0x7E407C02423C0000,	/* 5 */
	0x3C407C42423C0000,	/* 6 */
	0x7E04081020400000,	/* 7 */
	0x3C423C42423C0000,	/* 8 */
	0x3C42423E023C0000,	/* 9 */
	0x80000080000,		/* : */
	0x80000081000,		/* ; */
	0x6186018060000,	/* < */
	0x7E007E000000,		/* = */
	0x60180618600000,	/* > */
	0x3844041800100000,	/* ? */
	0x3C449C945C201C,	/* @ */
	0x1818243C42420000,	/* A */
	0x7844784444780000,	/* B */
	0x3844808044380000,	/* C */
	0x7844444444780000,	/* D */
	0x7C407840407C0000,	/* E */
	0x7C40784040400000,	/* F */
	0x3844809C44380000,	/* G */
	0x42427E4242420000,	/* H */
	0x3E080808083E0000,	/* I */
	0x1C04040444380000,	/* J */
	0x4448507048440000,	/* K */
	0x40404040407E0000,	/* L */
	0x4163554941410000,	/* M */
	0x4262524A46420000,	/* N */
	0x1C222222221C0000,	/* O */
	0x7844784040400000,	/* P */
	0x1C222222221C0200,	/* Q */
	0x7844785048440000,	/* R */
	0x1C22100C221C0000,	/* S */
	0x7F08080808080000,	/* T */
	0x42424242423C0000,	/* U */
	0x8142422424180000,	/* V */
	0x4141495563410000,	/* W */
	0x4224181824420000,	/* X */
	0x4122140808080000,	/* Y */
	0x7E040810207E0000,	/* Z */
	0x3820202020380000,	/* [ */
	0x4020100804020000,	/* \ */
	0x3808080808380000,	/* ] */
	0x1028000000000000,	/* ^ */
	0x7E0000,		/* _ */
	0x1008000000000000,	/* ` */
	0x3C023E463A0000,	/* a */
	0x40407C42625C0000,	/* b */
	0x1C20201C0000,		/* c */
	0x2023E42463A0000,	/* d */
	0x3C427E403C0000,	/* e */
	0x18103810100000,	/* f */
	0x344C44340438,		/* g */
	0x2020382424240000,	/* h */
	0x800080808080000,	/* i */
	0x800180808080870,	/* j */
	0x20202428302C0000,	/* k */
	0x1010101010180000,	/* l */
	0x665A42420000,		/* m */
	0x2E3222220000,		/* n */
	0x3C42423C0000,		/* o */
	0x5C62427C4040,		/* p */
	0x3A46423E0202,		/* q */
	0x2C3220200000,		/* r */
	0x1C201804380000,	/* s */
	0x103C1010180000,	/* t */
	0x2222261A0000,		/* u */
	0x424224180000,		/* v */
	0x81815A660000,		/* w */
	0x422418660000,		/* x */
	0x422214081060,		/* y */
	0x3C08103C0000,		/* z */
	0x1C103030101C0000,	/* { */
	0x808080808080800,	/* | */
	0x38080C0C08380000,	/* } */
	0x324C000000,		/* ~ */
	0x7E7E7E7E7E7E0000	/* DEL */
};

void
draw_text_len(char *text, int len, Vec2 pos, DColor color)
{
	for (int i = 0; i < len; i++) {

		Vec2 p = Vec2_add(pos, vec2(i*8, 0));

		uint64_t bit = draw_font[text[i]];
		for (int y = p.y; y < p.y + 8; y++) {

			uint8_t b = bit & 0xff;
			bit = bit >> 8;
			for (int x = p.x; x < p.x + 8; x++) {
				if (x < 0 || x >= S_WIDTH || y < 0 || y >= S_HEIGHT)
					continue;

				if (b & 0x80) screen[x + y * S_WIDTH] = color;
				b = b << 1;
			}
		}
	}
}

void
draw_text(char *text, Vec2 pos, DColor color)
{
	int len = 0;
	char *t = text;
	while(*t++) len++;
	draw_text_len(text, len, pos, color);
}

