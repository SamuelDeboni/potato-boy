#!/bin/bash

if [[ $1 = 'all' ]] ; then
    scp -r ../potato_boy/* pi@192.168.18.144:~/potato_boy/
fi

if [[ $1 = 'src' ]] ; then
    scp -r src/* pi@192.168.18.144:~/potato_boy/src/
fi
