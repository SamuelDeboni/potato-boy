#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>

#include "../input.h"
#include "../dmath.h"
#include "../draw.h"

#define MAP_W 16
#define MAP_H 16
static int mwalls[MAP_W * MAP_H] = {
	1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
	1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
	1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,
	1,0,0,0,0,0,0,0,0,0,0,6,6,6,0,1,
	1,2,2,2,2,2,0,0,0,0,0,0,0,0,0,1,
	1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
	1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
	1,3,3,3,0,0,0,0,0,0,0,0,0,0,0,1,
	1,4,4,4,0,0,0,0,0,5,5,5,0,5,5,1,
	1,0,0,0,0,0,0,0,0,5,0,0,0,0,0,1,
	1,0,0,0,0,0,0,0,0,5,0,0,0,0,0,1,
	1,0,0,0,7,0,0,0,0,5,0,0,0,0,0,1,
	1,0,0,7,7,0,0,0,0,5,0,0,0,0,0,1,
	1,0,0,0,0,0,0,0,0,5,0,0,0,0,0,1,
	1,0,0,0,0,0,0,0,0,5,0,0,0,0,0,1,
	1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
};

static Vec2 player_pos = {2, 2};
static float player_dir = 2;

static void
draw_rays()
{
	for (int i = 0; i < 128; i++) {
		float ray_angle = (float)(i - 64) / 128.0f;
		//ray_angle *= 1.25;
		ray_angle += player_dir;

		float rtan = tan(ray_angle);

		Vec2 tile_pos = Vec2_fract(player_pos);
		Vec2 ray_pos = player_pos;
		ray_pos.x = cos(ray_angle) >= 0 ? ceil(ray_pos.x) : floor(ray_pos.x) - 0.01;
		ray_pos.y += (ray_pos.x - player_pos.x) * rtan;

		// X
		int x_hit = 0;
		Vec2 x_hit_pos;

		Vec2 step;
		step.x = cos(ray_angle) >= 0 ? 1 : -1;
		step.y = step.x * rtan;
		while (ray_pos.x >= 0 && ray_pos.x < MAP_W
			&& ray_pos.y >= 0 && ray_pos.y < MAP_H)
		{
			int map_value = mwalls[(int)ray_pos.y * MAP_W + (int)ray_pos.x];
			if (map_value) {
				x_hit = map_value;
				x_hit_pos = Vec2_sub(ray_pos, player_pos);
				break;
			}
			ray_pos = Vec2_add(ray_pos, step);
		}

		ray_pos = player_pos;
		ray_pos.y = sin(ray_angle) >= 0 ? ceil(ray_pos.y) : floor(ray_pos.y) - 0.01;
		ray_pos.x += (ray_pos.y - player_pos.y) / rtan;

		// Y
		int y_hit = 0;
		Vec2 y_hit_pos;

		step.y = sin(ray_angle) >= 0 ? 1 : -1;
		step.x = step.y / rtan;
		while (ray_pos.x >= 0 && ray_pos.x < MAP_W
			&& ray_pos.y >= 0 && ray_pos.y < MAP_H)
		{
			int map_value = mwalls[(int)ray_pos.y * MAP_W + (int)ray_pos.x];
			if (map_value) {
				y_hit = map_value;
				y_hit_pos = Vec2_sub(ray_pos, player_pos);
				break;
			}
			ray_pos = Vec2_add(ray_pos, step);
		}

		float x_len = sqrt(x_hit_pos.x * x_hit_pos.x + x_hit_pos.y * x_hit_pos.y);
		float y_len = sqrt(y_hit_pos.x * y_hit_pos.x + y_hit_pos.y * y_hit_pos.y);

		if (!x_hit) x_len = 9999;
		if (!y_hit) y_len = 9999;

		int hit_on_x = x_len < y_len;

		int   hit  = hit_on_x ? x_hit : y_hit;
		float dist = hit_on_x ? x_len : y_len;

		if (dist < 0.01) dist = 0.01;

		float height = 100.0f / dist;
		height /= cos(ray_angle - player_dir);
		float p_height = height;
		if (height > 100) height = 100;

		uint16_t buff[64];
		DImage tex = get_sprite8(hit, buff);

		#define fract(x) (x - floor(x))

		float hit_t = hit_on_x ?
			fract(x_hit_pos.y + player_pos.y) :	fract(y_hit_pos.x + player_pos.x);

		for (int j = 0; j < height; j++) {
			int y = 110 - height * 0.5 + j;

			float u = hit_t;
			float v = (float)j / p_height + (p_height - height) * 0.5 / p_height;

			int bi = (int)((u) * 8) + (int)((v) * 8) * 8;
			if (bi < 0) bi = 0;
			if (bi > 63) bi = 63;
			DColor c = buff[bi];
			if (hit_on_x) {
				RGBColor c2 = color_to_rgb8(c);
				c2.r = (c2.r * 80) / 100;
				c2.g = (c2.g * 80) / 100;
				c2.b = (c2.b * 80) / 100;
				c = color_from_rgb8(c2.r, c2.g, c2.b);
			}

			screen[i + y * S_WIDTH] = c;
		}

		#undef fract
		/*
		draw_solid_rect((Rect){i, 110 - height * 0.5, 1, height},
			color_from_rgb8(50, 50, height * 2 + 50));
		*/
	}
}

void maze_start() {
	load_spritesheet("assets/bricks_spritesheet.tga");
}

void maze_update_and_render() {
	if (pressed[B_RIGHT]) player_dir += 0.1;
	if (pressed[B_LEFT])  player_dir -= 0.1;

	if (pressed[B_UP]) {
		player_pos.x += cos(player_dir) * 0.1;
		player_pos.y += sin(player_dir) * 0.1;
	}
	if (pressed[B_DOWN]) {
		player_pos.x -= cos(player_dir) * 0.1;
		player_pos.y -= sin(player_dir) * 0.1;
	}

	draw_solid_rect((Rect){0, 60, 128, 50}, color_from_rgb8(60, 60, 60));
	draw_solid_rect((Rect){0, 110, 128, 50}, color_from_rgb8(80, 150, 255));
	draw_rays();
}