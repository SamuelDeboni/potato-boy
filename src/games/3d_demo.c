#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>

#include "../input.h"
#include "../dmath.h"
#include "../draw.h"


void demo3d_start()
{
	load_spritesheet("assets/bricks_spritesheet.tga");
}


void demo3d_update_and_render()
{
	static float t = 0;
	t += 0.05;

	Mat4 m0 = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 3, 0, 1
	};

	Mat4 m1 = {
		 cos(t), sin(t), 0, 0,
		 -sin(t), cos(t), 0, 0,
		 0,      0,      1, 0,
		 0,      0,      0, 1
	};

	Mat4 m2 = {
		cos(t / 2), 0, -sin(t / 2), 0,
		0,      1, 0,       0,
		sin(t / 2), 0, cos(t / 2),  0,
		0,      0, 0,       1
	};

	Mat4 m = Mat4_mul(&m1, &m0);
	m = Mat4_mul(&m2, &m);
	set_matrix(m);

	start_3d_draw();
	set_projection(0.1, 100, 60);

	// front
	push_vertex(vec3(-1, -1, -1), vec3(1, 0, 0));
	push_vertex(vec3(-1, -1,  1), vec3(1, 0, 0));
	push_vertex(vec3( 1, -1, -1), vec3(1, 0, 0));

	push_vertex(vec3( 1, -1, -1), vec3(1, 0, 0));
	push_vertex(vec3(-1, -1,  1), vec3(1, 0, 0));
	push_vertex(vec3( 1, -1,  1), vec3(1, 0, 0));

	// back
	push_vertex(vec3(-1,  1,  1), vec3(1, 1, 0));
	push_vertex(vec3(-1,  1, -1), vec3(1, 1, 0));
	push_vertex(vec3( 1,  1, -1), vec3(1, 1, 0));

	push_vertex(vec3(-1,  1,  1), vec3(1, 1, 0));
	push_vertex(vec3( 1,  1, -1), vec3(1, 1, 0));
	push_vertex(vec3( 1,  1,  1), vec3(1, 1, 0));

	// right
	push_vertex(vec3(1, -1, -1), vec3(0, 1, 0));
	push_vertex(vec3(1, -1,  1), vec3(0, 1, 0));
	push_vertex(vec3(1,  1, -1), vec3(0, 1, 0));

	push_vertex(vec3(1,  1, -1), vec3(0, 1, 0));
	push_vertex(vec3(1, -1,  1), vec3(0, 1, 0));
	push_vertex(vec3(1,  1,  1), vec3(0, 1, 0));

	// left
	push_vertex(vec3(-1, -1,  1), vec3(0, 1, 1));
	push_vertex(vec3(-1, -1, -1), vec3(0, 1, 1));
	push_vertex(vec3(-1,  1, -1), vec3(0, 1, 1));

	push_vertex(vec3(-1, -1,  1), vec3(0, 1, 1));
	push_vertex(vec3(-1,  1, -1), vec3(0, 1, 1));
	push_vertex(vec3(-1,  1,  1), vec3(0, 1, 1));

	// up
	push_vertex(vec3( 1, -1, 1), vec3(0, 0, 1));
	push_vertex(vec3(-1, -1, 1), vec3(0, 0, 1));
	push_vertex(vec3(-1,  1, 1), vec3(0, 0, 1));

	push_vertex(vec3( 1, -1, 1), vec3(0, 0, 1));
	push_vertex(vec3(-1,  1, 1), vec3(0, 0, 1));
	push_vertex(vec3( 1,  1, 1), vec3(0, 0, 1));

	// down
	push_vertex(vec3(-1, -1, -1), vec3(1, 0, 1));
	push_vertex(vec3( 1, -1, -1), vec3(1, 0, 1));
	push_vertex(vec3(-1,  1, -1), vec3(1, 0, 1));

	push_vertex(vec3( 1, -1, -1), vec3(1, 0, 1));
	push_vertex(vec3( 1,  1, -1), vec3(1, 0, 1));
	push_vertex(vec3(-1,  1, -1), vec3(1, 0, 1));

	end_3d_draw();
}