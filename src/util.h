#ifndef UTIL_H
#define UTIL_H

#include <time.h>

static void
nsleep(long int time)
{
    struct timespec tim, tim2;
    tim.tv_sec = 0;
    tim.tv_nsec = time;
    nanosleep(&tim, &tim2);
}

#endif