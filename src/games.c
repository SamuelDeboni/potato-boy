// ==== Games ====
void breakout_start();
void breakout_update_and_render();

void snake_start();
void snake_update_and_render();

void bricks_start();
void bricks_update_and_render();

void shapes_start();
void shapes_update_and_render();

void maze_start();
void maze_update_and_render();

void demo3d_start();
void demo3d_update_and_render();

static int selected_game = 0;
static int game_selector = 1;
static int games_count = 6;

static char games_name[6][32] = {
	"Breakout",
	"Snake",
	"Potato Bricks",
	"Shapes",
	"Maze",
	"3d Demo",
};

// ===============

void start_games() {
	g_arena.offset = 0;
	switch (selected_game) {
		case 1: breakout_start(); break;
		case 2: snake_start(); break;
		case 3: bricks_start(); break;
		case 4: shapes_start(); break;
		case 5: maze_start(); break;
		case 6: demo3d_start(); break;
	}
}

void update_games() {
	switch (selected_game) {
		case 0: {
			if (pressed[B_DOWN] && !prev_pressed[B_DOWN]) {
				game_selector++;
			}
			if (pressed[B_UP] && !prev_pressed[B_UP]) {
				game_selector--;
			}

			if (game_selector > games_count)
				game_selector = 1;
			if (game_selector < 1)
				game_selector = games_count;

			int ui_y = S_HEIGHT - 10;

			draw_text("Potato Boy", vec2(22, ui_y),
				color_from_rgb8(200, 255, 0));
			ui_y -= 15;
			for (int i = 1; i <= games_count; i++) {
				if (i == game_selector) {
					draw_solid_rect(
						(Rect){0, ui_y, S_WIDTH, 10},
						color_from_rgb8(50, 50, 50));
				}

				draw_text(games_name[i-1],
					vec2(8, ui_y), 0xFFFF);
				ui_y -= 10;
			}

		    if (pressed[B_ONOFF]) {
		    	running = 0;
	        }

	        if (pressed[B_RESET]) {
	        	restart = 1;
	        }

	        if (pressed[B_ACTION] && !prev_pressed[B_ACTION])
	        {
	        	selected_game = game_selector;
	        	start_games();
	        }

		} break;
   		case 1: breakout_update_and_render(); break;
   		case 2: snake_update_and_render(); break;
   		case 3: bricks_update_and_render(); break;
   		case 4: shapes_update_and_render(); break;
   		case 5: maze_update_and_render(); break;
   		case 6: demo3d_update_and_render(); break;
	}
}
