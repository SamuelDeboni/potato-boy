#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "../input.h"
#include "../dmath.h"
#include "../draw.h"

#include "shapes/templates.c"

#define MOVE_BLOCK 1
#define CHAIN_TRANSACTION 2

typedef struct {
	uint32_t flags; 
	int block_id;
	IVec2 delta_pos;
} Transaction;

typedef struct {
	IVec2 pos;
} Player;

typedef enum {
	BlockType_Wall = 0,
	BlockType_Dummy,
	BlockType_Pipe,
	BlockType_Shape_Source,
	BlockType_Shape_Consumer,
	BlockType_Shape_Rotate_Right,
	BlockType_Shape_Rotate_Left,
	BlockType_Shape_Rotate_Union,
	BlockType_Shape_Rotate_Intersection,
	BlockType_Shape_Rotate_Diference,
} BlockType;

typedef struct {
	BlockType type;

	IVec2 pos;
	int sprite_id;
} Block;

typedef struct {
	Block blocks[256];
	int block_count;
} Level;

static Transaction pending_transactions[64];
static int pending_transactions_count;

static Transaction transactions[4096];
static int transaction_count;

static Player player;
static Level current_level;

static void
move_player(IVec2 dir) {
	pending_transactions[pending_transactions_count++] = (Transaction) {
		.flags = 0,
		.block_id = 0,
		.delta_pos = dir,
	};
}

static int
do_transaction(Transaction t)
{
	int result = 0;

	if (t.flags & MOVE_BLOCK) {
		if (t.block_id >= current_level.block_count) return 0;

		Block *b = &current_level.blocks[t.block_id];

		if (b->type == BlockType_Wall) return 0;

		IVec2 npos = IVec2_add(b->pos, t.delta_pos);

		int can_move = npos.x >= 0 && npos.x < 16 &&
			npos.y >= 0 && npos.y < 16;

		if (!can_move) return 0;

		int block_col_id = -1;
		for (int i = 0; i < current_level.block_count; i++) {
			if (i == t.block_id) continue;

			Block b2 = current_level.blocks[i];

			if (b2.pos.x == npos.x && b2.pos.y == npos.y) {
				block_col_id = i;
				break;
			}
		}

		if (block_col_id != -1) {
			Transaction new_t = t;
			new_t.flags = MOVE_BLOCK;	
			new_t.block_id = block_col_id;
			can_move = do_transaction(new_t);
			t.flags |= CHAIN_TRANSACTION;
		}

		if (can_move) {
			b->pos = npos;
			transactions[transaction_count++] = t;
			result = 1;
		}
	} else {
		IVec2 npos = IVec2_add(player.pos, t.delta_pos);
		int can_move = npos.x >= 0 && npos.x < 16 &&
			npos.y >= 0 && npos.y < 16;

		if (!can_move) return 0;

		int block_col_id = -1;
		for (int i = 0; i < current_level.block_count; i++) {
			Block b = current_level.blocks[i];

			if (b.pos.x == npos.x && b.pos.y == npos.y) {
				block_col_id = i;
				break;
			}
		}

		if (block_col_id != -1) {
			Transaction new_t = t;
			new_t.flags = MOVE_BLOCK;	
			new_t.block_id = block_col_id;
			can_move = do_transaction(new_t);
			t.flags |= CHAIN_TRANSACTION;
		}

		if (can_move)
		{
			player.pos = npos;
			transactions[transaction_count++] = t;
			result = 1;
		}
	}

	return result;
}

static void
do_transactions()
{
	for (int i = 0; i < pending_transactions_count; i++) {
		Transaction t = pending_transactions[i];
		do_transaction(t);
	}
	pending_transactions_count = 0;
}

static void
undo_transaction()
{
	if (transaction_count <= 0) return;

	while (1) {
		Transaction t = transactions[transaction_count - 1];
		transaction_count--;

		if (t.flags & MOVE_BLOCK) {
			if (t.block_id >= current_level.block_count) return;

			Block *b = &current_level.blocks[t.block_id];
			IVec2 npos = IVec2_sub(b->pos, t.delta_pos);
			b->pos = npos;
		} else {
			IVec2 npos = IVec2_sub(player.pos, t.delta_pos);
			player.pos = npos;
		}

		if (!(t.flags & CHAIN_TRANSACTION)) break;
	}
}

void shapes_start() {
	load_spritesheet("assets/shapes_spritesheet.tga");

	pending_transactions_count = 0;
	transaction_count = 0;
	player = (Player){0};
	player.pos = ivec2(3, 3);

	current_level.block_count = 0;
	for (int i = 0; i < 8; i++) {
		current_level.blocks[current_level.block_count++] = (Block) {
			.type = BlockType_Wall,
			.pos = {8, i},
			.sprite_id = 1,
		};
	}

	current_level.blocks[current_level.block_count++] = (Block) {
		.type = BlockType_Dummy,
		.pos = {9, 8},
		.sprite_id = 2,
	};

	current_level.blocks[current_level.block_count++] = (Block) {
		.type = BlockType_Dummy,
		.pos = {8, 9},
		.sprite_id = 3,
	};

	current_level.blocks[current_level.block_count++] = (Block) {
		.type = BlockType_Dummy,
		.pos = {10, 8},
		.sprite_id = 4,
	};

	for (int i = 0; i < 7; i++) {
		current_level.blocks[current_level.block_count++] = (Block) {
			.type = BlockType_Dummy,
			.pos = {4 + i, 12},
			.sprite_id = i + 16,
		};
	}
}

void shapes_update_and_render() {

	if (KEY_DOWN(B_UP))    move_player(ivec2( 0, 1));
	if (KEY_DOWN(B_DOWN))  move_player(ivec2( 0,-1));
	if (KEY_DOWN(B_RIGHT)) move_player(ivec2( 1, 0));
	if (KEY_DOWN(B_LEFT))  move_player(ivec2(-1, 0));

	do_transactions();

	if (KEY_DOWN(B_ACTION)) undo_transaction();

	// draw background
	draw_solid_rect((Rect){0, 0, 128, 128}, color_from_rgb8(60, 60, 60));

	// draw player
	draw_sprite8(ivec2_to_vec2(IVec2_mul(player.pos, ivec2e(8))), 0);

	// draw_blocks
	for (int i = 0; i < current_level.block_count; i++) {
		Block b = current_level.blocks[i];
		draw_sprite8(ivec2_to_vec2(IVec2_mul(b.pos, ivec2e(8))), b.sprite_id);
	}
}
