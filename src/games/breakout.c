#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "../input.h"
#include "../dmath.h"
#include "../draw.h"

#define BT_COUNT_X 8
#define BT_COUNT_Y 8

#define BT_W 16
#define BT_H 4

typedef struct {
    Vec2 pos;
    DColor color;
    int flag_destroy;
} BreakoutTile;

typedef struct {
    int count;
    BreakoutTile tiles[128];
} BreakoutTiles;


static Vec2 ball_pos;
static Vec2 ball_vel;
static Vec2 pad_pos;
static BreakoutTiles break_tiles;
static int lives = 5;

void
breakout_start()
{
    lives = 5;
	ball_pos = vec2(62, 20);
	ball_vel = vec2(2, 2);
	pad_pos = vec2(64, 10);

    break_tiles.count = 128;
    for (int i = 0; i < break_tiles.count; i++) {
        break_tiles.tiles[i].pos = vec2((i % 8) * 16, 144 - (i / 8) * 4);
        break_tiles.tiles[i].color =
            color_from_rgb8(rand() % 255, rand() % 255, rand() % 255);
        break_tiles.tiles[i].flag_destroy = 0;
    }
}

void
breakout_update_and_render()
{
	if (ball_pos.x <= 0 || ball_pos.x >= 124) {
    	ball_vel.x *= -1;
    }

    if (ball_pos.y <= 0 || ball_pos.y >= 156)
    {
    	ball_vel.y  *= -1;

        if (ball_pos.y <= 0) {
            lives -= 1;
            ball_pos = vec2(62, 20);
            ball_vel = vec2(2, 2);
        }
    }

    if (ball_pos.y <= pad_pos.y + 4 &&
        ball_pos.x >= pad_pos.x - 15 &&
        ball_pos.x <  pad_pos.x + 12 && ball_vel.y < 0)
    {
        ball_vel.y = 2;
        ball_vel.x += pressed[B_RIGHT] - pressed[B_LEFT];
        if (ball_vel.x > 2 || ball_vel.x < -2) {
            ball_vel.y = 1;
        }
        if (ball_vel.x > 3) ball_vel.x = 3;
        if (ball_vel.x < -3) ball_vel.x = -3;
    }

    // Tile colision    
    for (int i = 0; i < break_tiles.count; i++) {
        Vec2 tp = break_tiles.tiles[i].pos;

        // right collision
        int rc = (ball_pos.x >= tp.x - 4) && (ball_pos.x <= tp.x - 2)
                && (ball_pos.y >= tp.y - 4) && (ball_pos.y <= tp.y + BT_H) && ball_vel.x > 0;
        // left collision
        int lc = (ball_pos.x <= tp.x + BT_W) && (ball_pos.x >= tp.x + BT_W - 2) && (ball_pos.y >= tp.y - 4) && (ball_pos.y <= tp.y + BT_H) && ball_vel.x < 0;

        // vertival collision
        int vu = (ball_pos.x <= tp.x + BT_W) && (ball_pos.x >= tp.x - 4) &&
                (ball_pos.y >= tp.y - 4) && (ball_pos.y <= tp.y + BT_H) &&
                ball_vel.y > 0;
        int vd = (ball_pos.x <= tp.x + BT_W) && (ball_pos.x >= tp.x - 4) &&
                (ball_pos.y >= tp.y - 4) && (ball_pos.y <= tp.y + BT_H) &&
                ball_vel.y < 0;

        int coll = 0;
        if (rc || lc) {
            ball_vel.x *= -1;
            coll = 1;
        } else if (vu || vd) {
            ball_vel.y *= -1;
            coll = 1;
        }

        if (coll) {
            break_tiles.tiles[i].flag_destroy = 1;
        }
    }

    ball_pos = Vec2_add(ball_pos, ball_vel);

    if (pressed[B_RIGHT] && pad_pos.x < 112) pad_pos.x += 3;
    if (pressed[B_LEFT]  && pad_pos.x > 16)  pad_pos.x -= 3;

    // draw tiles
    {
        for (int i = 0; i < break_tiles.count; i++) {
            BreakoutTile tile = break_tiles.tiles[i];
            if (tile.flag_destroy && break_tiles.count > 0) {
                break_tiles.count--;
                break_tiles.tiles[i] = break_tiles.tiles[break_tiles.count];
                tile = break_tiles.tiles[i];
            }
        }

        for (int i = 0; i < break_tiles.count; i++) {
            BreakoutTile tile = break_tiles.tiles[i];
            draw_solid_rect((Rect){tile.pos.x, tile.pos.y, BT_W, BT_H},
                            tile.color);
        }
    }

    draw_solid_rect((Rect){pad_pos.x - 15, pad_pos.y, 30, 4},
    				color_from_rgb8(255, 0, 55));

    draw_solid_rect((Rect){ball_pos.x, ball_pos.y, 4, 4},
    				color_from_rgb8(50, 100, 255));

    for (int i = 0; i < lives; i++) {
        Rect rect = {4 + 8 * i, 160 - 8, 4, 4};
        draw_solid_rect(rect, color_from_rgb8(50, 100, 255));
    }

    if (break_tiles.count <= 0 || lives <= 0) breakout_start();
}