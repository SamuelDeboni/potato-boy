#ifndef INPUT_H
#define INPUT_H

#define B_ACTION 0
#define B_UP     1
#define B_RIGHT  2
#define B_ONOFF  3
#define B_RESET  4

#define B_DOWN   5
#define B_LEFT   6

#define B_START  7
#define B_SOUND  8

extern int pressed[9];
extern int prev_pressed[9];

#define KEY_DOWN(k) ( pressed[k] && !prev_pressed[k])
#define KEY_UP(k)   (!pressed[k] &&  prev_pressed[k])

#endif