#ifndef DRAW_H
#define DRAW_H

#include <sdf/sdf_mem.h>

#define S_WIDTH  128
#define S_HEIGHT 160
#define S_SIZE   (128*160)
#define SCREEN_PIXEL(x, y) (screen[(x) + (y) * S_WIDTH])
#define SPRITE8_X(sheet, idx) (((idx) % (sheet.w / 8)) * 8)
#define SPRITE8_Y(sheet, idx) (((idx) / (sheet.w / 8)) * 8)

#define DRAW_V_BUFFER_SIZE (8 * 1024)
typedef struct {
	Vec3 vpos[DRAW_V_BUFFER_SIZE];
	Vec3 vcol[DRAW_V_BUFFER_SIZE];
	int vbuffer_len; 

	Mat4 mat;
	float near, far, s;
} Draw3DContext;

typedef struct {
	int32_t w, h;	
	uint16_t *data;
} DImage;

extern uint16_t screen[S_WIDTH * S_HEIGHT];

void draw_solid_rect(Rect rect, DColor color);
void draw_text_len(char *text, int len, Vec2 pos, DColor color);
void draw_text(char *text, Vec2 pos, DColor color);
int load_spritesheet(char *path);
void draw_sprite8(Vec2 pos, int idx);
void draw_sprite8_scale(Vec2 pos, Vec2 scale, int idx);
DImage get_sprite8(int idx, uint16_t *mem);
void draw_line(Vec2 point_0, Vec2 point_1, DColor color);
void draw_solid_triangle(Vec2 p0, Vec2 p1, Vec2 p2, DColor color);
void draw_triangle(Vec2 p0, Vec2 p1, Vec2 p2, DColor color);

void start_3d_draw();
void push_vertex(Vec3 vertex, Vec3 color);
void set_matrix(Mat4 mat);
void set_projection(float near, float far, float fov);
void end_3d_draw();

#endif