#!/bin/bash

echo "building"

GAMES='src/games/3d_demo.c src/games/maze.c src/games/snake.c src/games/breakout.c src/games/bricks.c src/games/shapes.c'

flags='-D_GNU_SOURCE=1 -std=c11'
clang src/main_raylib.c lib/libraylib.a $flags -g -Iinclude $GAMES -o pbr -lGL -lm -lpthread -ldl -lrt -lX11  

if [[ $1 = 'run' ]] ; then
	echo "running"
	./pbr
fi
